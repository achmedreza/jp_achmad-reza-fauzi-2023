<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Rs_covid extends REST_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index_get(){
        $rs_covid = $this->hit_api_rs_rjk_covid();
        $rs = $this->hit_api_rsjkt();
        
        $rs_covid = json_decode($rs_covid);
        $rs = json_decode($rs);
//merge
        $rs_covid_baru = array();

        foreach ($rs_covid as $cov) {

            $found = false;
            foreach ($rs as $r) {
                $nama_rs_covid = str_replace("RSUD","Rumah Sakit Umum Daerah",$cov->nama_rumah_sakit);
                $nama_rs_covid = str_replace("RSKD","Rumah Sakit Khusus Jiwa",$nama_rs_covid);
                $nama_rs_covid = str_replace("RSAU","Rumah Sakit Umum Pusat Angkatan Udara",$nama_rs_covid);
                $nama_rs_covid = str_replace("RS","Rumah Sakit Umum",$nama_rs_covid);
                // $nama_rs_covid = $cov->nama_rumah_sakit;
                // $nama_rs = $r->jenis_rumah_sakit."".$r->nama_rs_covid;
                $nama_rs = $r->jenis_rumah_sakit." ".$r->nama_rumah_sakit;
                $nama_rs = str_replace("Rumah Sakit Umum Kecamatan Kelas D", "Rumah Sakit Umum Daerah",$nama_rs);

                $sim = similar_text(strtolower($nama_rs_covid) , strtolower($nama_rs), $perc);
                $sim2 = similar_text(strtolower($cov->alamat), strtolower($r->alamat_rumah_sakit), $perc2);
                $sim3 = similar_text(strtolower(str_replace(" ","",$cov->kelurahan)), strtolower(str_replace(" ","",$r->kelurahan)), $perc3);
                $sim4 = similar_text(strtolower($cov->kecamatan), strtolower($r->kecamatan), $perc4);
                

                if ($perc >= 81 && $perc2 >= 68 ) {
                    $p = array();
                    $p['nama_rumah_sakit'] = $cov->nama_rumah_sakit;
                    $p['jenis_rumah_sakit'] = $r->jenis_rumah_sakit;
                    $p['alamat_rumah_sakit'] = $r->alamat_rumah_sakit;
                    $p['kelurahan_rumah_sakit'] = $r->kelurahan;
                    $p['kecamatan_rumah_sakit'] = $r->kecamatan;
                    $p['kota_rumah_sakit'] = $cov->kota_madya;
                    $p['kodepos_rumah_sakit'] = $r->kode_pos;
                    $p['telepon_rumah_sakit'] = $r->nomor_telepon;
                    $p['fax_rumah_sakit'] = $r->nomor_fax;
                    $p['website_rumah_sakit'] = $r->website;
                    $p['email_rumah_sakit'] = $r->email;
                    // $p['perc'] = $perc;
                    // $p['perc2'] = $perc2;
                    // $p['perc3'] = $perc3;

                    // $rs_covid_new[] = $p;
                    $rs_covid_baru[] = $p;
                    $found = true;
                    // break();
                    break;
                } 
            }  

            if (!$found) {
                $p = array();
                $p['nama_rumah_sakit'] = $cov->nama_rumah_sakit;
                $p['jenis_rumah_sakit'] = null;
                $p['alamat_rumah_sakit'] = $cov->alamat;
                $p['kelurahan_rumah_sakit'] = $cov->kelurahan;
                $p['kecamatan_rumah_sakit'] = $cov->kecamatan;
                $p['kota_rumah_sakit'] = $cov->kota_madya;
                $p['kodepos_rumah_sakit'] = null;
                $p['telepon_rumah_sakit'] = null;
                $p['fax_rumah_sakit'] = null;
                $p['website_rumah_sakit'] = null;
                $p['email_rumah_sakit'] = null;
                // $p['perc'] = null;
                // $p['perc2'] = null;
                $rs_covid_baru[] = $p;
            }          
        }

        $kelurahan = strtolower($this->input->get("kelurahan", TRUE));
        $kecamatan = strtolower($this->input->get("kecamatan", TRUE));
        $kota = strtolower($this->input->get("kota", TRUE));
//filtering
        $new = array_filter($rs_covid_baru, function ($var) use ($kelurahan, $kecamatan, $kota) {
            return (strtolower($var['kelurahan_rumah_sakit']) == $kelurahan || strtolower($var['kecamatan_rumah_sakit']) == $kecamatan || strtolower($var['kota_rumah_sakit']) == $kota);
        });

        if ($new) {
            $this->set_response(
                array(
                    "status" => "Success",
                    "code" => 200,
                    "total" => count($new),
                    "data" => $new
            ), REST_Controller::HTTP_OK); 
        } else {
            $this->set_response(
                array(
                    "status" => "Success",
                    "code" => 200,
                    "total" => count($rs_covid_baru),
                    "data" => $rs_covid_baru
            ), REST_Controller::HTTP_OK); 
        }

    }

    public function hit_api_rs_rjk_covid(){
        $uri="https://data.jakarta.go.id/read-resource/get-json/daftar-rumah-sakit-rujukan-penanggulangan-covid-19/65d650ae-31c8-4353-a72b-3312fd0cc187"; 

        $headers = array( 
                "Accept: application/json", 
            );
        // $proxy = '10.15.3.20:80';
        $ch = curl_init($uri);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        $response = curl_exec($ch);
        return $response;
    }

    public function hit_api_rsjkt(){
        $uri="https://data.jakarta.go.id/read-resource/get-json/rsdkijakarta-2017-10/8e179e38-c1a4-4273-872e-361d90b68434"; 

        $headers = array( 
                "Accept: application/json", 
            );
        // $proxy = '10.15.3.20:80';
        $ch = curl_init($uri);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        $response = curl_exec($ch);
        return $response;
    }

}
