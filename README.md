HANDS-ON TEST
JP_Achmad Reza Fauzi

NOTE:
1. API ini dapat diakses tanpa menggunakan HTTP Request Headers
2. Untuk mengaksesnya , dapat diakses pada link URL {base_url}/rs_covid dengan metode GET
3. untuk mem-filter kelurahan , dapat menambahkan variabel kelurahan pada URL
contoh : {base_url}/rs_covid?kelurahan=Cideng
4. untuk filter kecamatan , dapat menambahkan variabel kecamatan pada URL
contoh : {base_url}/rs_covid?kecamatan=Gambir
5. untuk filter kota , dapat menambahkan variabel kota pada URL
contoh : {base_url}/rs_covid?kota=Jakarta Pusat
